======================
BWA Sequence Alignment
======================

Overview
========

This is an example GeneFlow bioinformatics workflow with two steps: 1) Reference index creation with BWA Index, and 2) Sequence alignment with BWA Mem.

Requirements
------------

Workflow steps require that a container system (Singularity or Docker) is available, or that each required tool (BWA) is available in the environment. 


Inputs
------

The workflow requires the following inputs.

1. files: Directory containing paired-end sequence files.

2. reference: Reference sequence FASTA file.

Parameters
----------

Workflow parameters can be specified, but default values are used otherwise.

1. threads: Number of CPU threads to use for alignment. Default: 2


